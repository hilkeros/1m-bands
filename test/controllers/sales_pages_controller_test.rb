require 'test_helper'

class SalesPagesControllerTest < ActionController::TestCase
  setup do
    @sales_page = sales_pages(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:sales_pages)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create sales_page" do
    assert_difference('SalesPage.count') do
      post :create, sales_page: { background_image: @sales_page.background_image, buy_button: @sales_page.buy_button, name: @sales_page.name, soundcloud: @sales_page.soundcloud }
    end

    assert_redirected_to sales_page_path(assigns(:sales_page))
  end

  test "should show sales_page" do
    get :show, id: @sales_page
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @sales_page
    assert_response :success
  end

  test "should update sales_page" do
    patch :update, id: @sales_page, sales_page: { background_image: @sales_page.background_image, buy_button: @sales_page.buy_button, name: @sales_page.name, soundcloud: @sales_page.soundcloud }
    assert_redirected_to sales_page_path(assigns(:sales_page))
  end

  test "should destroy sales_page" do
    assert_difference('SalesPage.count', -1) do
      delete :destroy, id: @sales_page
    end

    assert_redirected_to sales_pages_path
  end
end

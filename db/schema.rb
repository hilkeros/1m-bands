# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150810185742) do

  create_table "admins", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true
  add_index "admins", ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true

  create_table "bands", force: true do |t|
    t.string   "name"
    t.string   "short_name"
    t.string   "tumblr"
    t.string   "logo"
    t.string   "background_image"
    t.string   "host"
    t.string   "mailchimp_list"
    t.string   "mailchimp_buyers_list"
    t.string   "soundcloud_playlist"
    t.text     "facebook_conversion_code"
    t.string   "google_analytics_id"
    t.string   "mp3_download_link"
    t.string   "wav_download_link"
    t.string   "free_download_link"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "facebook_page"
  end

  create_table "fans", force: true do |t|
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "page_id"
  end

  create_table "pages", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.string   "background_image"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "list_name"
    t.integer  "band_id"
    t.text     "fb_headline"
    t.text     "fb_text"
    t.text     "fb_description"
  end

  create_table "sales_pages", force: true do |t|
    t.string   "name"
    t.string   "background_image"
    t.text     "soundcloud"
    t.text     "buy_button"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end

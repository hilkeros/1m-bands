# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

[
	{
		name: "Puppy Seeds",
		short_name: "puppy", 
		tumblr: "http://puppyseeds.tumblr.com", 
		logo: "http://hilk.eu/1mrecords/puppyseeds/PuppySeedsLogo.png", 
		background_image: "http://1mrecords.com/puppyseeds/puppyseeds1lores.jpeg", 
		host: "www.puppyseeds.co.uk",
		mailchimp_list: "2703a41b55",
		mailchimp_buyers_list: "896f757e7f",
		soundcloud_playlist: "107189257",
		facebook_conversion_code:  
			"<script>(function() {
			var _fbq = window._fbq || (window._fbq = []);
			if (!_fbq.loaded) {
			var fbds = document.createElement('script');
			fbds.async = true;
			fbds.src = '//connect.facebook.net/en_US/fbds.js';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(fbds, s);
			_fbq.loaded = true;
			}
			})();
			window._fbq = window._fbq || [];
			window._fbq.push(['track', '6026957183149', {'value':'0.01','currency':'EUR'}]);
			</script>
			<noscript><img height=\"1\" width=\"1\" alt=\"\" style=\"display:none\" src=\"https://www.facebook.com/tr?ev=6026957183149&amp;cd[value]=0.01&amp;cd[currency]=EUR&amp;noscript=1\" /></noscript>",
		google_analytics_id: "UA-60599470-7",
		mp3_download_link: "http://hilk.eu/1mrecords/puppyseeds/PuppySeedsEPmp3.zip",
		wav_download_link: "http://hilk.eu/1mrecords/puppyseeds/PuppySeedsEPwav.zip",
		free_download_link: "ForeverYoung.mp3"
	},{
		name: "U.V.",
		short_name: "uv", 
		tumblr: "http://uvmusic.tumblr.com", 
		logo: "http://hilk.eu/1mrecords/UV-LOGO-WHITE.png", 
		background_image: "http://hilk.eu/1mrecords/uv-press1.jpg", 
		host: "www.uvband.com",
		mailchimp_list: "5805f6c4fe",
		mailchimp_buyers_list: "8aabf59d39",
		soundcloud_playlist: "83898319",
		facebook_conversion_code:  
			"<script>(function() {
		      var _fbq = window._fbq || (window._fbq = []);
		      if (!_fbq.loaded) {
		        var fbds = document.createElement('script');
		        fbds.async = true;
		        fbds.src = '//connect.facebook.net/en_US/fbds.js';
		        var s = document.getElementsByTagName('script')[0];
		        s.parentNode.insertBefore(fbds, s);
		        _fbq.loaded = true;
		      }
		    })();
		    window._fbq = window._fbq || [];
		    window._fbq.push(['track', '6024062654949', {'value':'0.01','currency':'EUR'}]);
		    </script>
		    <noscript><img height=\"1\" width=\"1\" alt=\"\" style=\"display:none\" src=\"https://www.facebook.com/tr?ev=6024062654949&amp;cd[value]=0.01&amp;cd[currency]=EUR&amp;noscript=1\" /></noscript>",
		google_analytics_id: "UA-60599470-3",
		mp3_download_link: "http://hilk.eu/1mrecords/UV-EPmp3.zip",
		wav_download_link: "http://hilk.eu/1mrecords/UV-EPwav.zip",
		free_download_link: "Cuts.mp3"
	},{
		name: "YellowStraps",
		short_name: "yellowstraps", 
		tumblr: "http://yellowstraps.tumblr.com", 
		logo: "http://1mrecords.com/yellowstraps/YSwhiteLogoLores.png", 
		background_image: "http://1mrecords.com/yellowstraps/ys-headerimage.jpg", 
		host: "www.yellowstraps.be",
		mailchimp_list: "8503c0eeb0",
		mailchimp_buyers_list: "1b2a23698a",
		soundcloud_playlist: "88019550",
		facebook_conversion_code:  
			"<script>(function() {
			var _fbq = window._fbq || (window._fbq = []);
			if (!_fbq.loaded) {
			var fbds = document.createElement('script');
			fbds.async = true;
			fbds.src = '//connect.facebook.net/en_US/fbds.js';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(fbds, s);
			_fbq.loaded = true;
			}
			})();
			window._fbq = window._fbq || [];
			window._fbq.push(['track', '6026957746549', {'value':'0.01','currency':'EUR'}]);
			</script>
			<noscript><img height=\"1\" width=\"1\" alt=\"\" style=\"display:none\" src=\"https://www.facebook.com/tr?ev=6026957746549&amp;cd[value]=0.01&amp;cd[currency]=EUR&amp;noscript=1\" /></noscript>",
		google_analytics_id: "(UA-60599470-4",
		mp3_download_link: "http://hilk.eu/1mrecords/YellowStraps-WhirlwindEP-mp3.zip",
		wav_download_link: "http://hilk.eu/1mrecords/YellowStraps-WhirlWindRomanceEP-wav.zip",
		free_download_link: "YellowStraps-OfNoAvail.mp3"
	},{
		name: "XLMemories",
		short_name: "xl", 
		tumblr: "http://hroski.com", 
		logo: "http://hilk.eu/1mrecords/puppyseeds/PuppySeedsLogo.png", 
		background_image: "http://1mrecords.com/puppyseeds/puppyseeds1lores.jpeg", 
		host: "www.2manyxlmemories.com",
		mailchimp_list: "dcf4b882ed",
		mailchimp_buyers_list: "dcf4b882ed",
		soundcloud_playlist: "107189257",
		facebook_conversion_code:  
			"<script>(function() {
		      var _fbq = window._fbq || (window._fbq = []);
		      if (!_fbq.loaded) {
		        var fbds = document.createElement('script');
		        fbds.async = true;
		        fbds.src = '//connect.facebook.net/en_US/fbds.js';
		        var s = document.getElementsByTagName('script')[0];
		        s.parentNode.insertBefore(fbds, s);
		        _fbq.loaded = true;
		      }
		    })();
		    window._fbq = window._fbq || [];
		    window._fbq.push(['track', '6024062654949', {'value':'0.01','currency':'EUR'}]);
		    </script>
		    <noscript><img height=\"1\" width=\"1\" alt=\"\" style=\"display:none\" src=\"https://www.facebook.com/tr?ev=6024062654949&amp;cd[value]=0.01&amp;cd[currency]=EUR&amp;noscript=1\" /></noscript>",
		google_analytics_id: "UA-60599470-3",
		mp3_download_link: "http://hilk.eu/1mrecords/UV-EPmp3.zip",
		wav_download_link: "http://hilk.eu/1mrecords/UV-EPwav.zip",
		free_download_link: "Cuts.mp3"
	}
].each do |data|
	band = Band.find_by_short_name(data[:short_name])
	band ||= Band.new(data)
	band.update_attributes(data)
	band.save
	
end
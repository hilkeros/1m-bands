class CreateSalesPages < ActiveRecord::Migration
  def change
    create_table :sales_pages do |t|
      t.string :name
      t.string :background_image
      t.text :soundcloud
      t.text :buy_button

      t.timestamps
    end
  end
end

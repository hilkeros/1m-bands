class CreateBands < ActiveRecord::Migration
  def change
    create_table :bands do |t|
      t.string :name
      t.string :short_name
      t.string :tumblr
      t.string :logo
      t.string :background_image
      t.string :host
      t.string :mailchimp_list
      t.string :mailchimp_buyers_list
      t.string :soundcloud_playlist
      t.text :facebook_conversion_code
      t.string :google_analytics_id
      t.string :mp3_download_link
      t.string :wav_download_link
      t.string :free_download_link

      t.timestamps
    end
  end
end

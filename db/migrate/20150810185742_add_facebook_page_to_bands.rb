class AddFacebookPageToBands < ActiveRecord::Migration
  def change
    add_column :bands, :facebook_page, :string
  end
end

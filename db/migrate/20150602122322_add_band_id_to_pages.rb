class AddBandIdToPages < ActiveRecord::Migration
  def change
    add_column :pages, :band_id, :integer
  end
end

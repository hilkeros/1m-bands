class AddFacebookFieldsToPage < ActiveRecord::Migration
  def change
    add_column :pages, :fb_headline, :text
    add_column :pages, :fb_text, :text
    add_column :pages, :fb_description, :text
  end
end

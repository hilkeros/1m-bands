I created this app as an email marketing platform for the bands on my label 1M Records.

For each band we can create many different landing pages with an easy url in a quick way. On the landing page fans can leave their email address.

The app works with the Mailchimp API to subscribe fans to mailing lists.

There is also a simple page to sell music, making use of the Stripe API in a very quick way.


## Support & Documentation

Visit http://docs.c9.io for support, or to learn more about using Cloud9 IDE.
To watch some training videos, visit http://www.youtube.com/user/c9ide

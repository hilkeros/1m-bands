class SalesPagesController < ApplicationController
   before_action :authenticate_admin!, except: [:show, :done]

  respond_to :html

  #A very short cut to setting up a quick payment page with Stripe
  #Very bad practice to reveal my Stripe api key :-o

  def show
    respond_with(@sales_page)
  end

  def edit
  end

  def done
  	#This part contains Stripe example comments...
  	#The app goes over here when the Stripe form is sent

    # Set your secret key: remember to change this to your live secret key in production
    # See your keys here https://dashboard.stripe.com/account/apikeys
    Stripe.api_key = "sk_live_LJJcNxFy0wBK0ii86hpjUGnr"

    # Get the credit card details submitted by the form
    token = params[:stripeToken]

    # Create the charge on Stripe's servers - this will charge the user's card

    #The user is charged € 3,95 over here

    begin
      charge = Stripe::Charge.create(
        :amount => "0395", # amount in cents, again
        :currency => "eur",
        :source => token,
        :description => @band.name
      )
    rescue Stripe::CardError => e
      # The card has been declined
      # I didn't implement anything for errors....
    end
    #The user is redirect to the static thanks page
    redirect_to thanks_url
    #The user is subscribed to a different buyers list via the Mailchimp API
    email = params[:stripeEmail]
    source = params[:utm_source]
    Mailchimp.lists.subscribe(@band.mailchimp_buyers_list, {:email => email}, {'source' => source }, 'html', true, true)

  end

  private

    def sales_page_params
      params.require(:sales_page).permit(:name, :background_image, :soundcloud, :buy_button)
    end
end

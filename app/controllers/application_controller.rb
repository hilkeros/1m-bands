class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  #We can create landing pages for each band on its own domain.
  #All the domains are redirected to the 1M Heroku app.
  #By requesting the host the app finds out which band we are dealing with

  before_filter do
    @band = Band.find_by_host(request.host)
    if !@band
    	@band = Band.find_by_host("www." + request.host)
    end
  end

end

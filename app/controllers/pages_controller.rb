class PagesController < ApplicationController
  #This is an old controller which probably can be deleted
  #The pages controller works now in the bands folder

  before_action :set_page, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_admin!, only: [:index, :edit, :new, :update, :destroy]

  # GET /pages
  # GET /pages.json
  def index
    @pages = Page.all
    # if !@band
    #   @band = Band.find(params[:band_id])
    # end
  end

  # GET /pages/1
  # GET /pages/1.json
  def show
    @fan = Fan.new
  end

  # GET /pages/new
  def new
    @page = Page.new
    @current_band = @band.name
  end

  # GET /pages/1/edit
  def edit
  end

  # POST /pages
  # POST /pages.json
  def create
    @page = Page.new(page_params)
    @current_band = @band.id
    @page.update_attributes(:band_id => @current_band)
    respond_to do |format|
      if @page.save
        format.html { redirect_to @page, notice: 'Page was successfully created.' }
        format.json { render :show, status: :created, location: @page }
      else
        format.html { render :new }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pages/1
  # PATCH/PUT /pages/1.json
  def update
    respond_to do |format|
      if @page.update(page_params)
        format.html { redirect_to @page, notice: 'Page was successfully updated.' }
        format.json { render :show, status: :ok, location: @page }
      else
        format.html { render :edit }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pages/1
  # DELETE /pages/1.json
  def destroy
    @page.destroy
    respond_to do |format|
      format.html { redirect_to pages_url, notice: 'Page was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_page
      #@page = Page.friendly.find(params[:id])
      @page = Page.find_by(name: params[:id], band_id: @band.id)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def page_params
      params.require(:page).permit(:name, :description, :background_image, :fb_headline, :fb_text, :fb_description)
    end
end

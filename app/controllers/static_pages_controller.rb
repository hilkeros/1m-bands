class StaticPagesController < ApplicationController

  #The homepage of the band should live somewhere else on the net, typically a Tumbr
  #The root url ist redirected to this other website, which is defined in the band settings
  def home
      redirect_to @band.tumblr
  end

  #Fans get a free mp3 download after subscribing on a landing page
  #I didn't want the mp3 to be opened in the browser and created this hack to force the download
  #Unfortunately I have to host the mp3 in the public folder on Heroku. Not the best solution...

  def free_download
    send_file(
    "#{Rails.root}/public/#{@band.free_download_link}",
    filename: "#{@band.free_download_link}",
    type: "application/mp3"
    )
  end

  #A stub where Mailchimp takes users after they confirmed their email
  def confirm
  end

  #A thank you page after submitting your email. Next step is confirming the email which is sent by Mailchimp
  def thanks
  end

  #Funny url where the whole album can be downloaded after Stripe payment. Not safe at all of course.
  #Just come up with this url to download the music without paying
  def youareahero
  end
end

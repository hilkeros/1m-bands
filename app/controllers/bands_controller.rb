class BandsController < ApplicationController

	before_action :authenticate_admin!

	#CRUD for bands with a scaffold

	def index
		@bands = Band.all
	end

	def show
		@band = Band.find(params[:id])
	end

	def new
		@band = Band.new
	end

	def edit
		@band = Band.find(params[:id])
	end

	def create
		@band = Band.new(band_params)

		respond_to do |format|
			if @band.save
				format.html { redirect_to bands_path, notice: 'Band was succesfully created'}
			else
				format.html { render :new}
			end
		end
	end

	def update
		@band = Band.find(params[:id])
		respond_to do |format|
			if @band.update(band_params)
				format.html { redirect_to bands_path, notice: 'Band was succesfully updated'}
			else
				format.html { render :edit }
			end
		end
	end

	def destroy
		@band.destroy
		respond_to do |format|
			format.html { redirect_to to bands_url, notice: 'Band was succesfully destroyed' }
		end
	end

	private

	def set_band
		@band = Band.find(params[:id])
	end

	def band_params
		params.require(:band).permit(:name, :short_name, :tumblr, :logo, :background_image, :host, :mailchimp_list, :mailchimp_buyers_list, :soundcloud_playlist, :facebook_conversion_code, :google_analytics_id, :mp3_download_link, :wav_download_link, :free_download_link, :facebook_page)
	end
end

class Bands::PagesController < ApplicationController
  before_action :authenticate_admin!

  #CRUD for landing pages for each band. Made with scaffold

	def index
		@band = Band.find(params[:band_id])
	end

	def new
		band = Band.find(params[:band_id])
		@page = band.pages.build
  end

	def edit
		band = Band.find(params[:band_id])
    @page = Page.find_by(name: params[:id], band_id: band.id)
	end

	def create
    band = Band.find(params[:band_id])
    @page = band.pages.build(page_params)
    respond_to do |format|
      if @page.save
        format.html { redirect_to band_pages_path(band), notice: 'Page was successfully created.' }
        format.json { render :show, status: :created, location: @page }
      else
        format.html { render :new }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
     band = Band.find(params[:band_id])
     @page = Page.find_by(name: params[:id], band_id: band.id)
    respond_to do |format|
      if @page.update(page_params)
        format.html { redirect_to band_pages_path(band), notice: 'Page was successfully updated.' }
        format.json { render :show, status: :ok, location: @page }
      else
        format.html { render :edit }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    band = Band.find(params[:band_id])
    @page = Page.find_by(name: params[:id], band_id: band.id)
    @page.destroy
    respond_to do |format|
      format.html { redirect_to band_pages_path(band), notice: 'Page was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # # Use callbacks to share common setup or constraints between actions.
    def set_page
      #@page = Page.friendly.find(params[:id])
      #@page = Page.find_by(name: params[:id], band_id: @band.id)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def page_params
      params.require(:page).permit(:name, :description, :background_image, :band_id, :fb_headline, :fb_text, :fb_description)
    end

end

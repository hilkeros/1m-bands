json.array!(@sales_pages) do |sales_page|
  json.extract! sales_page, :id, :name, :background_image, :soundcloud, :buy_button
  json.url sales_page_url(sales_page, format: :json)
end

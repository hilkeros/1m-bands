json.array!(@pages) do |page|
  json.extract! page, :id, :name, :description, :background_image
  json.url page_url(page, format: :json)
end

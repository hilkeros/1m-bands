class Page < ActiveRecord::Base
    belongs_to :band
    has_many :fans
    validates :band_id, presence: true

    #Using the friendly id gem to create easy to remember landing pages. e.g. hroski.com/royal
    extend FriendlyId
    friendly_id :name
end
